import axios from "axios";
import transformation from "transform-coordinates";
import { queryPoint, searchAtPoint } from "./searchAtPoint";
import fs from "fs-extra";
import console from "console";

const transform = transformation("EPSG:4326", "3346");

async function fix() {
    const data = fs.readJsonSync("./project/ezerai-tvenkiniai.fix.geojson");
    console.log(data.features.length);
    for (let iF = 0; iF < data.features.length; iF++) {
        const feature = data.features[iF];

        console.log(iF, "/", data.features.length);
        if (!feature.properties.lat) {
            const x = feature.properties.OBJEKTOLKS;
            const y = feature.properties.OBJEKTOL_1;
            const [lng, lat] = transform.inverse([x, y])
            const p = await queryPoint({ lat, lng }, "savivaldybes");
            if (p) {
                console.log("rado naudosim ", lat, lng)
                feature.properties.lat = lat;
                feature.properties.lng = lng;
            } else {
                const [lng, lat] = data.features[0].geometry.coordinates[0][0][0];
                console.log("nerado naudosim ", lat, lng)
                feature.properties.lat = lat;
                feature.properties.lng = lng;
            }
            fs.writeJsonSync("./project/ezerai-tvenkiniai.fix.geojson", data);

        } else {
            console.log("turi lat", feature.properties.lat)
        }
    }
}

fix();

// const url = "http://localhost:3748/?service=WFS&version=1.0.0&request=GetFeature&typeName=ezerai-tvenkiniai&outputFormat=application/json";
// axios.get(url).then(async res => {
//     const transform = transformation("EPSG:4326", "3346");
//     const features = res.data.features;
//     let noPOints = 0;
//     for (let iF = 0; iF < features.length; iF++) {
//         const f = features[iF];
//         const x = f.properties.OBJEKTOLKS;
//         const y = f.properties.OBJEKTOL_1;
//         const [lat, lng] = transform.inverse([x, y])
//         const p = await queryPoint({ lat, lng }, "ezerai-tvenkiniai");
//         if (!p) {
//             noPOints = noPOints + 1;
//         } else {

//         }
//         console.log(iF, "/", features.length, "noPoints", noPOints);

//     }


//     debugger;
// });