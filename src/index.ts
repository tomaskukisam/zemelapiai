import express from 'express';


import { getFusedSearch, simpleString } from "./search";
import { searchAtPoint } from './searchAtPoint';





const initSearch = async () => {
    return {
        baraiMariose: await getFusedSearch("name", "zvejybosBaraiMariose"),
        upesKanalai: await getFusedSearch("PAVADINIMA", "upes-kanalai"),
        savivaldybes: await getFusedSearch("SAV_PAV", "savivaldybes"),
        ezeraiTvenkiniai: await getFusedSearch("PAVADINIMA", "ezerai-tvenkiniai"),
    }
}


const init = async () => {
    const fuses = await initSearch();
    console.log("barai inited", fuses.baraiMariose.search("Bar").length > 0);
    console.log("upes kanalai inited", fuses.upesKanalai.search("Nemun").length > 0);
    console.log("savivaldybes inited", fuses.savivaldybes.search("plu").length > 0);
    console.log("ezeraiTvenkiniai inited", fuses.ezeraiTvenkiniai.search("galv").length > 0);

    const app = express();
    const port = 6017;

    app.get("/search/:query", async (req, res) => {
        try {
            const start = Date.now();
            const search = simpleString(req.params.query || "");
            if (search.length < 2) {
                return {
                    baraiMariose: [],
                    upesKanalai: [],
                    savivaldybes: [],
                    ezeraiTvenkiniai: [],
                }
            }
            const limit = parseInt(req.query.limit + "") || 10;
            const results = {
                baraiMariose: (await fuses.baraiMariose.search(search)).slice(0, limit),
                upesKanalai: (await fuses.upesKanalai.search(search)).slice(0, limit),
                savivaldybes: (await fuses.savivaldybes.search(search)).slice(0, limit),
                ezeraiTvenkiniai: (await fuses.ezeraiTvenkiniai.search(search)).slice(0, limit),
            }
            const end = Date.now();
            console.log(search, "took", end - start, "ms");
            res.json(results);
        } catch (error) {
            console.log(error);
            //@ts-ignore
            res.json({ error: error.message }).status(500);
        }

    });
    app.listen(port, () => console.log(`started http://localhost:${port}/search/galve`));
};

init();
