import axios from 'axios';
import {
    getBoundingBox
} from 'geolocation-utils'
interface Coordinates { lat: number, lng: number }''
export const getBox = (coordinates: Coordinates, tolerance: number = 0.0002) => {
    const delta = tolerance;

    const topLeft = {
        lng: coordinates.lng - delta,
        lat: coordinates.lat + delta,
    };
    const bottomRight = {
        lng: coordinates.lng + delta,
        lat: coordinates.lat - delta,
    };
    const box = `${topLeft.lng},${bottomRight.lat},${bottomRight.lng},${topLeft.lat}`;
    return box;
};

export const searchAtPoint = async (coordinates: Coordinates, layer: string = "savivaldybes") => {

    return {
        // savivaldybe: await queryPoint(coordinates, "savivaldybes") || undefined,
        //   upeKanalas: await queryPoint(coordinates, "upes-kanalai") || undefined,
        ezerasTvenkinys: await queryPoint(coordinates, "ezerai-tvenkiniai") || undefined,
        // barasMarios: await queryPoint(coordinates, "zvejybosBaraiMariose") || undefined,
    }
};

export const queryPoint = async (coordinates: Coordinates, layer: string = "savivaldybes") => {
    const boundBox = getBoundingBox([coordinates], 1);
    //@ts-ignore
    const box = `${boundBox.bottomRight.lat},${boundBox.topLeft.lng},${boundBox.topLeft.lat},${boundBox.bottomRight.lng}`
    const url = `http://localhost:3748/?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&BBOX=${box}&CRS=EPSG:4326&WIDTH=1315&HEIGHT=1159&LAYERS=${layer}&STYLES=&FORMAT=image/jpeg&QUERY_LAYERS=${layer}&INFO_FORMAT=application/json&I=462&J=479&FEATURE_COUNT=10`;
    return ((await axios.get(url))?.data?.features || [])[0];
};