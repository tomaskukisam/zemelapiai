import Fuse from "fuse.js";
import axios from 'axios';
import centroid from "@turf/centroid";
import { polygon } from "@turf/turf";
export const simpleString = (param: string): string => {
    return param.toLowerCase().replaceAll("ą", "a").replaceAll("č", "c").replaceAll("ę", "e").replaceAll("ė", "e").replaceAll("į", "i").replaceAll("š", "s").replaceAll("ų", "u").replaceAll("ū", "u").replaceAll("ž", "z");
}
export const getFusedSearch = async (propName: string = "name", typeName = "zvejybosBaraiMariose") => {
    console.log("starting fuse for " + typeName);
    const url = `http://qgisserver:80/?service=WFS&version=1.0.0&request=GetFeature&typeName=${typeName}&outputFormat=application/json`
    console.log(url);
    const data = (await axios.get(url)).data;

    //@ts-ignore
    data.features = data.features.map((f, i) => {
        if (!f.properties.lat) {
            try {
                const center = centroid(polygon(typeName === "savivaldybes" ? f.geometry.coordinates[0] : f.geometry.coordinates));
                f.properties.lat = center.geometry.coordinates[1];
                f.properties.lng = center.geometry.coordinates[0];
            } catch (error) {
                console.log("error", error);

                debugger;
            }

        };



        const titles: string = simpleString(f.properties[propName]);
        f.title = titles;
        titles.split(" ").forEach((t, i) => {
            //@ts-ignore
            f["title" + i] = t;
        });
        return f;
    });

    const options = {
        // isCaseSensitive: false,
        includeScore: true,
        shouldSort: true,
        includeMatches: true,
        // findAllMatches: false,
        // minMatchCharLength: 1,
        location: 0,

        threshold: 0.1,
        distance: 0,
        // useExtendedSearch: false,
        // ignoreLocation: false,
        // ignoreFieldNorm: false,
        keys: [
            "title",
            "title0",
            "title1",
            "title2",
            "title3",
            "title4",
            "title5"
        ]
    };
    const fuse = new Fuse(data.features, options);
    console.log("started fuse for " + typeName);
    return fuse;
};