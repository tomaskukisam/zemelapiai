import fs from "fs-extra";

function getEuclideanDistance(pointA: { x: number, y: number }, pointB: { x: number, y: number }) {
    const distanceX = pointA.x - pointB.x;
    const distanceY = pointA.y - pointB.y;

    return Math.sqrt(distanceX * distanceX + distanceY * distanceY);
};

function getPolylineLength(points: Array<{ x: number, y: number }>) {
    let distance = 0;

    points.forEach((point, index) => {
        if (typeof points[index + 1] === 'undefined') return;
        distance += getEuclideanDistance(point, points[index + 1]);
    });

    return distance;
}
function getPointsCharacteristics(points: Array<{ x: number, y: number }>) {
    const distanceToMiddlePoint = getPolylineLength(points) / 2;

    if (distanceToMiddlePoint === 0) {
        return points[0];
    }

    if (distanceToMiddlePoint < 0 || points.length < 2) {
        return null;
    }

    let distanceToNextPoint = 0;
    let distanceToPreviousPoint = 0;
    let pointIndex = 0;

    for (let i = 1; i <= points.length; i += 1) {
        distanceToPreviousPoint = distanceToNextPoint;

        distanceToNextPoint += getEuclideanDistance(points[i], points[i - 1]);

        if (distanceToNextPoint >= distanceToMiddlePoint) {
            pointIndex = i;
            break;
        }
    }

    if (distanceToNextPoint < distanceToMiddlePoint) {
        return null;
    }

    return {
        previousPoint: points[pointIndex - 1],
        nextPoint: points[pointIndex],
        distanceToPreviousPoint,
        distanceToNextPoint,
        distanceToMiddlePoint
    };
}
function getPolylineMiddlePointCoordinates(points: Array<{ x: number, y: number }>) {
    //@ts-ignore
    const { previousPoint, nextPoint, distanceToPreviousPoint, distanceToNextPoint, distanceToMiddlePoint } = getPointsCharacteristics(points);

    const vectorScalingFactor = (distanceToMiddlePoint - distanceToPreviousPoint) / (distanceToNextPoint - distanceToPreviousPoint);

    const point = {
        x: previousPoint.x + (nextPoint.x - previousPoint.x) * vectorScalingFactor,
        y: previousPoint.y + (nextPoint.y - previousPoint.y) * vectorScalingFactor
    };
    let returnPoint = points[0];

    points.forEach((iPoint, index) => {
        const oldDistance = getEuclideanDistance(point, returnPoint);
        const distance = getEuclideanDistance(point, iPoint);
        if (oldDistance > distance) {
            returnPoint = iPoint;
        }
    });
    return returnPoint;
}

async function fixUpes() {
    const data = fs.readJsonSync("./project/upes-kanalai.fix.geojson");
    //upes-kanalai.fix.geojson
    console.log(data.features.length);

    for (let iF = 0; iF < data.features.length; iF++) {
        console.log(iF, "/", data.features.length);
        const feature = data.features[iF];
        if (!feature.properties.lat) {
            const line: Array<{ x: number, y: number }> = [];
            //@ts-ignore
            feature.geometry.coordinates[0].map(c => {
                //@ts-ignore

                //@ts-ignore
                line.push({ x: c[0], y: c[1] });

            });
            const xy = getPolylineMiddlePointCoordinates(line);

            feature.properties.lat = xy.y;
            feature.properties.lng = xy.x;
        } else {
            console.log("skipping", iF);
        }


    }
    fs.writeJsonSync("./project/upes-kanalai.fix.geojson", data);
}

fixUpes();