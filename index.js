const express = require('express');
const app = express();
const PORT = 3007;
var proxy = require('express-http-proxy');
app.use(express.static('public'));
app.use('/', proxy('http://localhost:8080'));

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));